import collections


class Frame(object):
    """Represents a "frame" of audio data."""
    def __init__(self, bytes, start, end):
        self.bytes = bytes
        self.start = start
        self.end = end


def frame_generator(frame_duration_ms, pcm_iterator, sample_rate):
    """Generates audio frames from PCM audio data.
    Takes the desired frame duration in milliseconds, the PCM data, and
    the sample rate.
    Yields Frames of the requested duration.
    """
    n = int(sample_rate * (frame_duration_ms / 1000.0) * 2)
    timestamp = 0
    duration = int(n / 2)
    pcm = b""
    for speech in pcm_iterator:
        pcm += speech.bin
        offset = 0
        while offset + n < len(pcm):
            yield Frame(pcm[offset:offset + n], timestamp, timestamp + duration)
            timestamp += duration
            offset += n
        pcm = pcm[offset:]


def vad_collector(sample_rate, num_start_padding_frames, num_end_padding_frames, vad, frames):
    """Filters out non-voiced audio frames.
    Given a webrtcvad.Vad and a source of audio frames, yields only
    the voiced audio.
    Uses a padded, sliding window algorithm over the audio frames.
    When more than 90% of the frames in the window are voiced (as
    reported by the VAD), the collector triggers and begins yielding
    audio frames. Then the collector waits until 90% of the frames in
    the window are unvoiced to detrigger.
    The window is padded at the front and back to provide a small
    amount of silence or the beginnings/endings of speech around the
    voiced frames.
    Arguments:
    sample_rate - The audio sample rate, in Hz.
    frame_duration_ms - The frame duration in milliseconds.
    padding_duration_ms - The amount to pad the window, in milliseconds.
    vad - An instance of webrtcvad.Vad.
    frames - a source of audio frames (sequence or generator).
    Returns: A generator that yields PCM audio data.
    """
    # We use a deque for our sliding window/ring buffer.
    strat_ring_buffer = collections.deque(maxlen=num_start_padding_frames)
    end_ring_buffer = collections.deque(maxlen=num_end_padding_frames)
    # We have two states: TRIGGERED and NOTTRIGGERED. We start in the
    # NOTTRIGGERED state.
    triggered = False

    voiced_frames = []
    for frame in frames:
        is_speech = vad.is_speech(frame.bytes, sample_rate)

        # sys.stdout.write('1' if is_speech else '0')
        if not triggered:
            strat_ring_buffer.append((frame, is_speech))
            num_voiced = len([f for f, speech in strat_ring_buffer if speech])
            # If we're NOTTRIGGERED and more than 90% of the frames in
            # the ring buffer are voiced frames, then enter the
            # TRIGGERED state.
            if num_voiced > 0.9 * strat_ring_buffer.maxlen:
                triggered = True
                # sys.stdout.write('+(%s)' % (ring_buffer[0][0].timestamp,))
                # We want to yield all the audio we see from now until
                # we are NOTTRIGGERED, but we have to start with the
                # audio that's already in the ring buffer.
                for f, s in strat_ring_buffer:
                    voiced_frames.append(f)
                strat_ring_buffer.clear()
        else:
            # We're in the TRIGGERED state, so collect the audio data
            # and add it to the ring buffer.
            voiced_frames.append(frame)
            end_ring_buffer.append((frame, is_speech))
            num_unvoiced = len([f for f, speech in end_ring_buffer if not speech])
            # If more than 90% of the frames in the ring buffer are
            # unvoiced, then enter NOTTRIGGERED and yield whatever
            # audio we've collected.
            if num_unvoiced > 0.9 * end_ring_buffer.maxlen:
                # sys.stdout.write('-(%s)' % (frame.timestamp + frame.duration))
                triggered = False
                yield voiced_frames[0].start, voiced_frames[-1].end
                end_ring_buffer.clear()
                voiced_frames = []
    # if triggered:
    #     sys.stdout.write('-(%s)' % (frame.timestamp + frame.duration))
    # sys.stdout.write('\n')
    # If we have any leftover voiced audio when we run out of input,
    # yield it.
    if voiced_frames:
        yield voiced_frames[0].start, voiced_frames[-1].end
