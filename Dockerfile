FROM ubuntu:18.04

RUN apt-get update && \
    apt-get install -y build-essential python3 python3-pip && \
    apt-get install -y nano && \
    apt-get clean && \
    apt-get autoremove && \
    rm -rf /var/lib/apt/lists/* /tmp/* /workspace/* && \
    python3 -m pip --no-cache-dir install \
        grpcio==1.13.0 \
        grpcio-tools==1.13.0 \
        protobuf==3.5.1 \
        six==1.13.0 \
        webrtcvad==2.0.10

RUN mkdir -p /root/vad

COPY . /root/vad

RUN cd /root/vad && \
    python3 -m grpc.tools.protoc --proto_path=. --python_out=. --grpc_python_out=. vad.proto
