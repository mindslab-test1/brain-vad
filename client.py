#!/usr/bin/env python3.5
# -*- coding: UTF-8 -*-

import argparse
import grpc
import wave

from vad_pb2_grpc import VadStub
from vad_pb2 import Speech


def read_pcm(wav_file):
    with wave.open(wav_file, 'rb') as rf:
        pcm_binary = bytearray()
        while True:
            data = rf.readframes(16 * 1024)
            if len(data) == 0:
                break
            pcm_binary.extend(data)
    pcm_binary = bytes(pcm_binary)
    return pcm_binary


class VadClient(object):
    def __init__(self, remote='127.0.0.1:14001', chunk_size=1024*1024):
        self.channel = grpc.insecure_channel(remote)
        self.stub = VadStub(self.channel)
        self.chunk_size = chunk_size

    def detect(self, pcm_binary):
        pcm_binary = self._generate_wav_binary_iterator(pcm_binary)
        return self.stub.Detect(pcm_binary)

    def detect_wav(self, wav_binary):
        wav_binary = self._generate_wav_binary_iterator(wav_binary)
        return self.stub.DetectWav(wav_binary)

    def _generate_wav_binary_iterator(self, pcm_binary):
        for idx in range(0, len(pcm_binary), self.chunk_size):
            yield Speech(bin=pcm_binary[idx:idx+self.chunk_size])

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.channel.close()


def _main():
    parser = argparse.ArgumentParser(
        description='vad client')
    parser.add_argument('-r', '--remote',
                        nargs='?',
                        dest='remote',
                        help='grpc: ip:port',
                        type=str,
                        default='127.0.0.1:14001')
    parser.add_argument('-i', '--input',
                        nargs='?',
                        help='input wav file',
                        type=str,
                        required=True)
    args = parser.parse_args()

    with VadClient(args.remote) as client:
        print("pcm")
        pcm_binary = read_pcm(args.input)
        for segment in client.detect(pcm_binary):
            print(segment.start, segment.end)

        print("wav")
        with open(args.input, 'rb') as rf:
            wav_binary = rf.read()
            for segment in client.detect_wav(wav_binary):
                print(segment.start, segment.end)


if __name__ == '__main__':
    _main()
